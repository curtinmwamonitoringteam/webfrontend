
export let lineChartOptions = {
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  title: {
    display: true,
    text: 'default title',
  },
  elements: {
    line: {
      tension: 0,
      borderWidth: 0.5,
      borderColor: 'rgba(255, 255, 255, 1.0)',
      fill: false,
      cubicInterpolationMode: 'monotone',
    },
    point: {
      radius: 0,
      hoverRadius: 8,
      hitRadius: 4,
      borderColor: 'rgba(200, 200, 200, 0.5)',
    },
  },
  scales: {
    yAxes: [
      {
        display: true,
        ticks: {
          min: 0,
          max: 30,
        }
      }
    ],
  },
};


export let shortNames = new Map();
shortNames.set('AllOn', 'on');
shortNames.set('All_0', 'on');
shortNames.set('AllOff', 'off');
shortNames.set('Dipole01_0', 'A');
shortNames.set('Dipole02_0', 'B');
shortNames.set('Dipole03_0', 'C');
shortNames.set('Dipole04_0', 'D');
shortNames.set('Dipole05_0', 'E');
shortNames.set('Dipole06_0', 'F');
shortNames.set('Dipole07_0', 'G');
shortNames.set('Dipole08_0', 'H');
shortNames.set('Dipole09_0', 'I');
shortNames.set('Dipole10_0', 'J');
shortNames.set('Dipole11_0', 'K');
shortNames.set('Dipole12_0', 'L');
shortNames.set('Dipole13_0', 'M');
shortNames.set('Dipole14_0', 'N');
shortNames.set('Dipole15_0', 'O');
shortNames.set('Dipole16_0', 'P');
