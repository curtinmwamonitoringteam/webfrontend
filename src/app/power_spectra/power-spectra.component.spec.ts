import { PowerSpectraComponent } from './power-spectra.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DipoleService } from '../repository/dipole.service';
import { AnomalyService } from '../repository/anomaly.service';
import { RibbonModule } from '../ribbon/ribbon.module';
import { PowerSpectraCardComponent } from './power-spectra-card.component';
import { ChartsModule } from 'ng2-charts';
import { TestService } from '../repository/test.service';
import { httpFactory, HttpInterceptor } from '../interceptor/http-interceptor';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import 'rxjs';

describe('Power Spectra Component', () => {
  let component: PowerSpectraComponent;
  let fixture: ComponentFixture<PowerSpectraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        RibbonModule,
        ChartsModule,
      ],
      declarations: [
        PowerSpectraComponent,
        PowerSpectraCardComponent,
      ],
      providers: [
        AnomalyService,
        DipoleService,
        TestService,
        {provide: HttpInterceptor, useFactory: httpFactory, deps: [XHRBackend, RequestOptions]}
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PowerSpectraComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component instanceof PowerSpectraComponent)
      .toBe(true, 'Failed to create Power Spectra Component');
  });


  it('should resize', () => {
    spyOn(component, 'updateCardSpace');
    component.resizeCards(50);
    expect(component.updateCardSpace).toHaveBeenCalled();
  });

  it('should fail resize', () => {
    spyOn(component, 'updateCardSpace');
    component.resizeCards(-1);
    expect(component.updateCardSpace).not.toHaveBeenCalled();
  });

});