import { Component } from '@angular/core';
import { RibbonGroup, RibbonGroupType } from '../ribbon/ribbon-group.class';
import { TestService } from '../repository/test.service';
import { ChartData } from './chart-data.class';
import { DipoleService } from '../repository/dipole.service';
import { AnomalyService } from '../repository/anomaly.service';

@Component({
  selector: 'mwa-power-spectra',
  templateUrl: 'power-spectra.component.html',
  styleUrls: ['power-spectra.component.css'],
})
export class PowerSpectraComponent {

  cardSize: number;   // dimensions of a single card
  rowSize: number;    // cards per row
  rows: number;
  chartDataList: ChartData[] = [];

  ribbonData: RibbonGroup[];

  constructor(
    private testService: TestService,
    private dipoleService: DipoleService,
    private anomalyService: AnomalyService
  ) {
    this.testService
      .getTestsByDateWithObservations(1159917656)
      .subscribe(data => {
        for (let ii = 0; ii < 16; ii++ ) {
          this.chartDataList.push(new ChartData(
            data[ii],
            dipoleService,
            anomalyService
          ));
        }
      });
  }

  ngOnInit(): void {
    // init data used to populate the ribbon for this page
    this.ribbonData = [
      new RibbonGroup('Filter Options', RibbonGroupType.LABEL, 105, [
        {label: 'Filter Options:'}
      ]),
      new RibbonGroup('Filters', RibbonGroupType.BUTTON, 365, [
        {label: 'Date Range', func: () => {}},
        {label: 'Report No', func: () => {}},
        {label: 'Anomaly Type', func: () => {}},
      ]),
      new RibbonGroup('Size', RibbonGroupType.BUTTON, 190, [
        {label: 'Small', func: () => this.resizeCards(400)},
        {label: 'Medium', func: () => this.resizeCards(600)},
        {label: 'Large', func: () => this.resizeCards(800)},
      ]),
    ];

    this.resizeCards(400);
  }

  resizeCards(size: number): void {
    if (size > 0) {
      // determine how many cards will fit in the card space
      this.cardSize = size;
      this.rowSize = Math.floor(document.getElementById("cardSpace").clientWidth / this.cardSize);
      this.rows = Math.floor(document.getElementById("cardSpace").clientHeight / this.cardSize);
      this.updateCardSpace();
    }
  }

  updateCardSpace() {
  }

}
