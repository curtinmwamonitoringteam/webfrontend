import { Test } from '../model/test.class';
import { shortNames } from './power-spectra-card-config';
import { DipoleService } from '../repository/dipole.service';
import { AnomalyService } from '../repository/anomaly.service';
import { Dipole } from '../model/dipole.class';
import { Anomaly } from '../model/anomaly.class';

export class ChartData {

  id: number;

  // chart vars
  xPolarityData: any[] = [];
  yPolarityData: any[] = [];

  showAllOn: boolean = false;
  showAllOff: boolean = false;

  public hasData: boolean = false;

  constructor(
    private test: Test,
    private dipoleService: DipoleService,
    private anomalyService: AnomalyService
  ) {
    this.id = test.id;

    // convert observation values to decibels
    let obs_id_list: number[] = [];
    for (let obs of this.test.observations) {
      ChartData.convertToDecibels(obs.values);
      obs_id_list.push(obs.id); // get obs IDs to get related anomalies later
    }

    // process data ready for drawing
    this.dipoleService
      .getDipoles(this.test.tile_id)
      .do(dipoles => this.splitData(dipoles))
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(obs_id_list))
      .subscribe(anomalies => {
        this.setAnomalies(anomalies);
        // ready to draw
        this.hasData = true;
      });
  }


  // converts an array of numbers to unit decibels
  // sets negative elements to zero
  static convertToDecibels(values: number[]) {
    for (let ii in values) {
      if (values[ii] < 0) {
        values[ii] = 0.0;
      }
      else {
        values[ii] = 10 * Math.log10(values[ii] + 1);
      }
    }
  }


  // split data into x and y data sets
  private splitData(dipoles: Dipole[]) {
    // get x polarity dipoles
    let xPolDipoles = dipoles.filter(dipole => dipole.polarity === 0);

    for (let obs of this.test.observations) {
      // X polarity
      if (xPolDipoles.some(x => obs.dipole_id === x.id)) {
        this.xPolarityData.push({
          obsID: obs.id,
          data: obs.values,
          label: shortNames.get(obs.name),
          showLine: this.showLine(obs.name),
        });
      }

      // Y polarity
      else {
        this.yPolarityData.push({
          obsID: obs.id,
          data: obs.values,
          label: shortNames.get(obs.name),
          showLine: this.showLine(obs.name),
        });
      }
    }
  }


  // takes a list of anomalies and flags corresponding observation data sets as anomalous
  private setAnomalies(anomalies: Anomaly[]) {
    for (let anom of anomalies) {
      let obs = this.xPolarityData.find((x: any) => x.obsID === anom.observation);
      if (obs == null) {
        obs = this.yPolarityData.find((x: any) => x.obsID === anom.observation);
      }
      if (obs != null) {
        obs.anomaly = true;
        obs.borderColor = 'rgba(255, 0, 0, 1.0)';
      }
      else {
        console.log('unmatched anomaly in power-spectra-card.component.ts : setAnomalies()')
      }
    }
  }


  // hide/show the AllOn and AllOff observations
  private showLine(name: string) {
    if (name == 'AllOn' || name == 'All_0') {
      return this.showAllOn;
    }
    if (name == 'AllOff') {
      return this.showAllOff;
    }
    return true;
  }

}

