import { PowerSpectraCardComponent } from './power-spectra-card.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import 'rxjs';
import { ChartsModule } from 'ng2-charts';
import { httpFactory, HttpInterceptor } from '../interceptor/http-interceptor';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { Test } from '../model/test.class';
import { Observation } from '../model/observation.class';
import { ChartData } from './chart-data.class';
import { AnomalyService } from '../repository/anomaly.service';
import { DipoleService } from '../repository/dipole.service';
import { ReflectiveInjector } from '@angular/core';

describe('Power Spectra Card Component', () => {
  let component: PowerSpectraCardComponent;
  let fixture: ComponentFixture<PowerSpectraCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        ChartsModule,
      ],
      declarations: [
        PowerSpectraCardComponent,
      ],
      providers: [
        AnomalyService,
        DipoleService,
        {provide: HttpInterceptor, useFactory: httpFactory, deps: [XHRBackend, RequestOptions]}
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PowerSpectraCardComponent);
    component = fixture.componentInstance;

    let obs = new Observation();
    obs.id = 1;
    obs.name = 'Dipole01_0';
    obs.start_time = 1234567890;
    obs.dipole_id = 1;
    obs.values = [0.1, 0.2, 3.3, 4.6, 5.9];

    let test = new Test();
    test.id = 1;
    test.tile_id = 11;
    test.start_time = 1;
    test.observations = [obs];

    let injector = ReflectiveInjector.resolveAndCreate([
      AnomalyService,
      DipoleService,
      {provide: HttpInterceptor, useFactory: httpFactory, deps: [XHRBackend, RequestOptions]},
    ]);
    component.chartData = new ChartData(test, injector.get(DipoleService), injector.get(AnomalyService));

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component instanceof PowerSpectraCardComponent)
      .toBe(true, 'Failed to create Power Spectra Card Component');
  });

});
