import { Component, Input } from '@angular/core';
import { lineChartOptions } from './power-spectra-card-config';
import { ChartData } from './chart-data.class';

@Component({
  selector: 'mwa-power-spectra-card',
  templateUrl: 'power-spectra-card.component.html',
  styleUrls: ['power-spectra-card.component.css'],
})
export class PowerSpectraCardComponent {

  // card data
  @Input() width: number;
  @Input() height: number;
  @Input() chartData: ChartData;

  // chart vars
  lineChartLabelX: number[] = [];
  lineChartType = 'line';

  // remove ng-chart default colour options
  lineChartColours = [{}];

  // deep copy options
  lineChartOptionsX = JSON.parse(JSON.stringify(lineChartOptions));
  lineChartOptionsY = JSON.parse(JSON.stringify(lineChartOptions));


  ngOnInit() {
    // init labels for the x axis
    for (let ii = 0; ii < 256; ii++) {
      this.lineChartLabelX.push(ii);
    }
    // set chart titles
    this.lineChartOptionsX.title.text = 'Tile ' + this.chartData.id.toString() + ' X';
    this.lineChartOptionsY.title.text = 'Tile ' + this.chartData.id.toString() + ' Y';
  }

}
