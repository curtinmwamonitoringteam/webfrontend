import {NgModule} from '@angular/core';
import {PowerSpectraComponent} from './power-spectra.component';
import {PowerSpectraCardComponent} from "./power-spectra-card.component";
import { BrowserModule } from '@angular/platform-browser';
import { RibbonModule } from '../ribbon/ribbon.module';
import { TestService } from '../repository/test.service';
import { ChartsModule } from 'ng2-charts';
import { DipoleService } from '../repository/dipole.service';
import { AnomalyService } from '../repository/anomaly.service';

@NgModule({
  imports: [
    BrowserModule,
    RibbonModule,
    ChartsModule,
  ],
  declarations: [
    PowerSpectraComponent,
    PowerSpectraCardComponent,
  ],
  exports: [
    PowerSpectraComponent,
  ],
  providers: [
    AnomalyService,
    DipoleService,
    TestService,
  ],
})
export class PowerSpectraModule {

}