
import { ChartData } from './chart-data.class';
import { Observation } from '../model/observation.class';
import { Test } from '../model/test.class';

describe('Chart Data Class', () => {

  beforeEach(() => {
    let obs = new Observation();
    obs.id = 1;
    obs.name = 'Dipole01_0';
    obs.start_time = 1234567890;
    obs.dipole_id = 1;
    obs.values = [0.1, 0.2, 3.3, 4.6, 5.9];

    let test = new Test();
    test.id = 1;
    test.tile_id = 11;
    test.start_time = 1;
    test.observations = [obs];
  });

  it('should convert to decibels', () => {
    let values = [1, 8.2, -1.0];

    ChartData.convertToDecibels(values);

    expect(Number(values[0].toFixed(2))).toBe(3.01);
    expect(Number(values[1].toFixed(2))).toBe(9.64);
    expect(Number(values[2].toFixed(2))).toBe(0);
  });

});
