import { Component } from '@angular/core';
import {EmailService} from "../repository/email.service";
import {Email} from "../model/email.class";

@Component({
  selector: 'mwa-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
})
export class HomeComponent {
  private subscribers: Email[] = [];

  constructor(private emailService: EmailService) {
    this.getAllSubscribers();
  }

  getAllSubscribers() {
    this.emailService.getAllSubscribers()
      .subscribe(data => {
        this.subscribers = data;
      })
  }

  showQuerySubscriber() {
    if(document.getElementById("email-address-container").style.getPropertyValue("visibility") == "hidden") {
      document.getElementById("email-address-container").style.setProperty("visibility", "visible");
    } else {
      document.getElementById("email-address-container").style.setProperty("visibility", "hidden");
    }
  }

  saveInput() {
    document.getElementById("email-address-container").style.setProperty("visibility", "hidden");

    let email_address = (<HTMLInputElement>document.getElementById("email-input")).value;

    if(email_address != null && email_address.length > 0 && email_address.indexOf("@") != -1) {
      let email = this.subscribers.find((email) => email.email_address == email_address);
      if(email) {
        alert("Already subscribed!");
      } else {
        this.addSubscriber(email_address);
      }
    } else {
      alert("Email address invalid!");
    }
  }

  deleteEmail(email_address: string) {
    let result = confirm("Delete " + email_address + "?");
    if(result) {
      this.emailService.deleteSubscriber(email_address)
        .subscribe(data => {
          if (data.status == 200) {
            alert("Subscriber deleted successfully");
            this.getAllSubscribers();
          } else {
            alert("Could not delete subscriber");
          }
        });
    }
  }

  addSubscriber(email_address: string) {
    this.emailService.addSubscriber(email_address)
      .subscribe(data => {
        if(data.status == 201) {
          alert("Subscriber added successfully");
          this.getAllSubscribers();
        }
      });
  }
}