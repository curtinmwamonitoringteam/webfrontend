import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class RoutingService {

  constructor(
    private router: Router,
  ) {
  }

  routeTo(route: string): void {
    this.router.navigateByUrl(route);
  }

}