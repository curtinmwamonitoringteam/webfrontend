import { async, fakeAsync, TestBed } from "@angular/core/testing";
import { Router } from '@angular/router';
import { RoutingService } from './routing.service';

describe('Routing Service', () => {
  let fixture: RoutingService;
  let routerStub: any;

  beforeEach(async(() => {
    routerStub = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };

    TestBed.configureTestingModule({
      providers: [
        {provide: Router, useValue: routerStub},
      ]
    });

    fixture = new RoutingService(routerStub);
  }));

  it('navigateByUrl called correctly', fakeAsync(() => {
    fixture.routeTo('test');
    expect(routerStub.navigateByUrl).toHaveBeenCalledWith('test');
  }));

});
