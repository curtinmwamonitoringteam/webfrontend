import { Component } from '@angular/core';
import { RoutingService } from './routing.service';

@Component({
  selector: 'mwa-navigation-header',
  templateUrl: 'navigation-header.component.html',
  styleUrls: ['navigation-header.component.css'],
})
export class NavigationComponent {

  navButtons: {label: string, route: string}[] = new Array();

  constructor(
    private routingService: RoutingService,
  ) {
  }

  routeTo(route: string): void {
    this.routingService.routeTo(route);
  }

  ngOnInit(): void {
    this.navButtons.push({label:'Power Spectra', route:'power-spectra'});
    this.navButtons.push({label:'Health Reports', route:'health-reports'});
    this.navButtons.push({label:'Statistics', route:'statistics'});
  }
}
