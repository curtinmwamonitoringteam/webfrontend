import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationComponent } from './navigation-header.component';
import { RouterTestingModule } from '@angular/router/testing'
import { AppRoutingModule } from './routing.module';
import { RoutingService } from './routing.service';
import { AppModule } from '../app.module';

describe('Navigation Header Component', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppRoutingModule,
        RouterTestingModule,
        AppModule,
      ],
      declarations: [
      ],
      providers: [
        RoutingService,
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(NavigationComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component instanceof NavigationComponent).toBe(true, 'Failed to create NavigationComponent');
  });


});
