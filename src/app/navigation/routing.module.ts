import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { PowerSpectraComponent } from '../power_spectra/power-spectra.component';
import { HealthReportsComponent } from '../health_reports/health-reports.component';
import { StatisticsComponent } from '../statistics/statistics.component';
import { HealthReportsSingularComponent } from "../health_reports/health-report-singular.component";
import { StatisticsTileDetailsComponent } from "../statistics/statistics-tile-details.component";

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'power-spectra', component: PowerSpectraComponent},
  {path: 'health-reports', component: HealthReportsComponent},
  {path: 'health-report-singular', component: HealthReportsSingularComponent},
  {path: 'statistics', component: StatisticsComponent},
  {path: 'statistics-tile-details',component: StatisticsTileDetailsComponent},
  {path: '**', redirectTo: 'home'}, // undefined routes redirect to home, need a 404 page
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule {
}