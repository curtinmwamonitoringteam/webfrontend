import { Component } from '@angular/core';
import { RibbonGroup, RibbonGroupType } from '../ribbon/ribbon-group.class';
import { TestService } from '../repository/test.service';
import { Test } from "../model/test.class";
import { AnomalyService } from "../repository/anomaly.service";
import { Anomaly } from "../model/anomaly.class";
import {DipoleService} from "../repository/dipole.service";
import {Dipole} from "../model/dipole.class";
import {TileService} from "../repository/tile.service";
import {Tile} from "../model/tile.class";

@Component({
  selector: 'mwa-health-report',
  templateUrl: 'health-reports.component.html',
  styleUrls: ['health-reports.component.css'],
})


export class HealthReportsComponent {
  sorted_testlist: TestRow[] = [];
  ribbon_data: RibbonGroup[];
  test_list_data: Test[] = [];
  dipoles: Dipole[] = [];
  tiles: Tile[] = [];
  obs_id_list: number[] = [];

  // Test ID for singular report
  static testid: string;

  // GPS Time start in UTC seconds
  private readonly GPS_START: number = 315964782;
  private readonly MONTH_IN_SECONDS: number = 2629743;
  private readonly months: string[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  private readonly dipole_names: string[] = ["AllOn", "AllOff", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "AllOff", "AllOn"];

  // Default date range for test service
  private start_date_GPS: number = (Math.trunc(Date.now()/1000) + this.GPS_START) - (this.MONTH_IN_SECONDS * 2);
  private end_date_GPS: number = Math.trunc(Date.now()/1000) + this.GPS_START;

  private all_tests: boolean = true;

  constructor(private testService: TestService, private anomalyService: AnomalyService, private dipoleService: DipoleService, private tileService: TileService) {
    this.getTestsByDateRange(this.start_date_GPS, this.end_date_GPS);
  }

  ngOnInit(): void {
    this.ribbon_data = [
      new RibbonGroup('Filter Options', RibbonGroupType.LABEL, 105, [
        {label: 'Filter Options:'}
      ]),
      new RibbonGroup('Filters', RibbonGroupType.BUTTON, 400, [
        {label: 'Date Range', func: this.showDateRange},
        {label: 'Report No', func: this.showIdRange},
        {label: 'Reset Filters', func: this.removeFilters}
      ]),
    ];
  }

  saveId(id: string) {
    HealthReportsComponent.testid = id;
  }

  getTestList(): TestRow[] {
    return this.sorted_testlist;
  }

  showDateRange() {
    if(document.getElementById("date-range-container").style.getPropertyValue("visibility") == "hidden") {
      // If id range div is visible, hide it when date range is clicked
      if(document.getElementById("id-range-container").style.getPropertyValue("visibility") == "visible") {
        document.getElementById("id-range-container").style.setProperty("visibility", "hidden");
      }
      document.getElementById("date-range-container").style.setProperty("visibility", "visible");
    } else {
      document.getElementById("date-range-container").style.setProperty("visibility", "hidden");
    }
  }

  showIdRange() {
    if(document.getElementById("id-range-container").style.getPropertyValue("visibility") == "hidden") {
      // If date range div is visible, hide it when id range is clicked
      if(document.getElementById("date-range-container").style.getPropertyValue("visibility") == "visible") {
        document.getElementById("date-range-container").style.setProperty("visibility", "hidden");
      }
      document.getElementById("id-range-container").style.setProperty("visibility", "visible");
    } else {
      document.getElementById("id-range-container").style.setProperty("visibility", "hidden");
    }
  }

  removeFilters() {
    this.sorted_testlist = [];
    // Reset dates
    this.start_date_GPS = (Math.trunc(Date.now()/1000) + this.GPS_START) - (this.MONTH_IN_SECONDS * 2);
    this.end_date_GPS = Math.trunc(Date.now()/1000) + this.GPS_START;

    this.getTestsByDateRange(this.start_date_GPS, this.end_date_GPS);
  }

  filterByDateRange() {
    // Get input values
    let start_input = (<HTMLInputElement>document.getElementById("date-input-start")).value;
    let end_input = (<HTMLInputElement>document.getElementById("date-input-end")).value;

    // Convert input values to date
    let start_date = new Date(start_input);
    let end_date = new Date(end_input);

    // If input is empty, and all_test is false, reset table
    if(start_input == "" && end_input == "" && this.all_tests == false) {
      this.showDateRange();
      this.sorted_testlist = [];
      this.getTestsByDateRange(this.start_date_GPS, this.end_date_GPS);
      this.all_tests = true;
    } else if(start_input != "" && end_input != ""){
      if (start_date <= end_date) {
        // Hide date range div on press of confirm
        document.getElementById("date-range-container").style.setProperty("visibility", "hidden");

        // Convert dates to GPS time
        this.start_date_GPS = start_date.getTime() / 1000 - this.GPS_START;
        this.end_date_GPS = end_date.getTime() / 1000 - this.GPS_START;

        // Clear table
        this.sorted_testlist = [];

        // Rebuild table
        this.getTestsByDateRange(this.start_date_GPS, this.end_date_GPS);

        // Set all_test to false for view all button
        this.all_tests = false;
      } else {
        alert("Start date can't be after end date!");
      }
    }
  }

  filterByIdRange() {
    // Get input values
    let start_input = (<HTMLInputElement>document.getElementById("id-input-start")).value;
    let end_input = (<HTMLInputElement>document.getElementById("id-input-end")).value;

    // Converts the input value to number
    let start_id = +start_input;
    let end_id = +end_input;

    // If input is empty, and all_test is false, reset table
    if(start_input == "" && end_input == "" && this.all_tests == false) {
      this.showIdRange();
      this.sorted_testlist = [];
      this.getTestsByDateRange(this.start_date_GPS, this.end_date_GPS);
      this.all_tests = true;
    } else if(start_input != "" && end_input != "") {
      if (start_id <= end_id) {
        // Hide id range div on press of confirm
        document.getElementById("id-range-container").style.setProperty("visibility", "hidden");

        // Clear table
        this.sorted_testlist = [];

        // Rebuild table
        this.getTestsByIdRange(start_id, end_id);

        // Set all_test to false for view all button
        this.all_tests = false;
      } else {
        alert("Start ID can't be less than end ID!");
      }
    }
  }

  // Function that fills the table using a date range
  getTestsByDateRange(start_date_GPS: number, end_date_GPS: number) {
    // Get tests from specified range of date
    this.testService.getTestsByDateRangeWithObservations(start_date_GPS, end_date_GPS)
      .do(data => {
        this.test_list_data = data;

        // Get all observation IDs from each test
        for(let test of this.test_list_data) {
          for(let obs of test.observations) {
            this.obs_id_list.push(obs.id);
          }
        }
      })
      .switchMap(() => this.dipoleService.getAllDipoles())
      .do(data => {
        this.dipoles = data;
      })
      .switchMap(() => this.tileService.getAllTiles())
      .do(data => {
        this.tiles = data;
      })
      // Get anomalies using all observation ids from tests
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.obs_id_list))
      .subscribe(data => {
        for(let test of this.test_list_data) {
          let anomalous_dipoles: {dipole: string, polarity: string}[] = [];

          // Filter the data from backend to only contain one test's anomalies
          let anomalies: Anomaly[] = data.filter((anomaly) => {
            for( let obs of test.observations){
              if(obs.id == anomaly.observation)
                return true;
            }
          });

          // Filter observations from current test to only contain anomalous observations
          let filtered_obs = test.observations.filter((obs) => {
            for(let anomaly of anomalies) {
              if(anomaly.observation == obs.id)
                return true;
            }
          });

          // Filter the dipoles to only contain anomalous dipoles
          let dipoles = this.dipoles.filter((dipole) => {
            for(let obs of filtered_obs) {
              if(obs.dipole_id == dipole.id)
                return true;
            }
          });

          // Convert start_time to Date/Time
          let start_time = new Date(0);
          start_time.setUTCSeconds(test.start_time + this.GPS_START);

          let start_time_string: string =
            this.months[start_time.getMonth()] +
            ' ' + start_time.getDate() +
            ', ' + start_time.getFullYear() +
            ' ' + start_time.getHours() +
            ':' + start_time.getMinutes() +
            ':' + start_time.getSeconds();

          for(let dipole of dipoles) {
            anomalous_dipoles.push({
              dipole: this.dipole_names[dipole.dipole_number],
              polarity: (dipole.polarity == 0) ? "X" : "Y"
            });
          }

          let tile = this.tiles.find((tile) => tile.id == test.tile_id);

          let testRow: TestRow = new TestRow(test.id, tile.name, start_time_string, anomalous_dipoles);

          this.sorted_testlist.push(testRow);
        }
      })
  }

  // Function that fills the table using test id range
  getTestsByIdRange(start_id: number, end_id: number) {
    // Get tests from specified range of id
    this.testService.getTestsByIdRangeWithObservations(start_id, end_id)
      .do(data => {
        this.test_list_data = data;

        // Get all observation IDs from each test
        for(let test of this.test_list_data) {
          for(let obs of test.observations) {
            this.obs_id_list.push(obs.id);
          }
        }
      })
      // Get anomalies using all observation ids from tests
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.obs_id_list))
      .subscribe(data => {
        for(let test of this.test_list_data) {
          let anomalous_dipoles: {dipole: string, polarity: string}[] = [];

          // Filter the data from backend to only contain one test's anomalies
          let anomalies: Anomaly[] = data.filter((anomaly) => {
            for( let obs of test.observations){
              if(obs.id == anomaly.observation)
                return true;
            }
          });

          // Filter observations from current test to only contain anomalous observations
          let filtered_obs = test.observations.filter((obs) => {
            for(let anomaly of anomalies) {
              if(anomaly.observation == obs.id)
                return true;
            }
          });

          // Filter the dipoles to only contain anomalous dipoles
          let dipoles = this.dipoles.filter((dipole) => {
            for(let obs of filtered_obs) {
              if(obs.dipole_id == dipole.id)
                return true;
            }
          });

          // Convert start_time to Date/Time
          let start_time = new Date(0);
          start_time.setUTCSeconds(test.start_time + this.GPS_START);

          let start_time_string: string =
            this.months[start_time.getMonth()] +
            ' ' + start_time.getDate() +
            ', ' + start_time.getFullYear() +
            ' ' + start_time.getHours() +
            ':' + start_time.getMinutes() +
            ':' + start_time.getSeconds();

          for(let dipole of dipoles) {
            anomalous_dipoles.push({
              dipole: this.dipole_names[dipole.dipole_number],
              polarity: (dipole.polarity == 0) ? "X" : "Y"
            });
          }

          let tile = this.tiles.find((tile) => tile.id == test.tile_id);

          let testRow: TestRow = new TestRow(test.id, tile.name, start_time_string, anomalous_dipoles);

          this.sorted_testlist.push(testRow);
        }
      })
  }
}

// Container class for each row, for sorting purposes
export class TestRow {
  tileID: string;
  id: number;
  dateTime: string;
  anomDipoles: {dipole: string, polarity: string}[];

  constructor(id: number, tileID: string, dateTime: string, anomDipoles: {dipole: string, polarity: string}[]) {
    this.id = id;
    this.tileID = tileID;
    this.dateTime = dateTime;
    this.anomDipoles = anomDipoles;
  }

  getID(): number {
    return this.id;
  }

  getTileID(): string {
    return this.tileID;
  }

  getDateTime(): string {
    return this.dateTime;
  }

  getAnomDipoles(): string {
    let anomString: string = "";
    for(let dipole of this.anomDipoles) {
      if(this.anomDipoles.indexOf(dipole) == this.anomDipoles.length-1) {
        anomString += dipole.dipole.toString() + "_" + dipole.polarity;
      } else {
        anomString += dipole.dipole.toString() + "_" + dipole.polarity + ", ";
      }
    }
    return anomString;
  }
}