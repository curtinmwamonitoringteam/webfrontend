import {HealthReportsSingularComponent} from "./health-report-singular.component";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {ConnectionBackend, HttpModule, RequestOptions, XHRBackend} from "@angular/http";
import {RibbonModule} from "../ribbon/ribbon.module";
import {RibbonComponent} from "../ribbon/ribbon.component";
import {AnomalyService} from "../repository/anomaly.service";
import {TestService} from "../repository/test.service";
import {httpFactory, HttpInterceptor} from "../interceptor/http-interceptor";
import {AnomalyTypeService} from "../repository/anomalyType.service";
import {DipoleService} from "../repository/dipole.service";

describe('Health Reports Singular Component', () => {
  let component: HealthReportsSingularComponent;
  let fixture: ComponentFixture<HealthReportsSingularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        RibbonModule,
      ],
      declarations: [
        HealthReportsSingularComponent,
        RibbonComponent,
      ],
      providers: [
        TestService,
        AnomalyService,
        AnomalyTypeService,
        DipoleService,
        HttpInterceptor,
        ConnectionBackend,
        {provide: HttpInterceptor, userFactory: httpFactory, deps: [XHRBackend, RequestOptions]}
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(HealthReportsSingularComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
    }));

  it('should create right component', () => {
    expect(component instanceof HealthReportsSingularComponent)
      .toBe(true, 'should create HealthReportsSingularComponent')
  });
});