import { Component } from '@angular/core';
import { HealthReportsComponent } from "./health-reports.component";
import { Test } from "../model/test.class";
import { Dipole } from "../model/dipole.class";
import { AnomalyService } from "../repository/anomaly.service";
import { AnomalyTypeService } from "../repository/anomalyType.service";
import { RoutingService } from "../navigation/routing.service";
import { TestService } from "../repository/test.service";
import { DipoleService } from "../repository/dipole.service";
import { Observation } from "../model/observation.class";
import { Anomaly } from "../model/anomaly.class";
import { AnomalyType } from "../model/anomaly-type.class";
import {TileService} from "../repository/tile.service";
import {Tile} from "../model/tile.class";

@Component({
  selector: 'mwa-health-report-singular',
  templateUrl: 'health-report-singular.component.html',
  styleUrls: ['health-report-singular.component.css'],
})

export class HealthReportsSingularComponent {
  private test_id: number;

  private total_dipole: number = 20;
  private anomaly_counter_x: number = 0;
  private anomaly_counter_y: number = 0;

  private current_nonfunc_pct_x = 0;
  private current_nonfunc_pct_y = 0;
  private current_func_pct_x = 0;
  private current_func_pct_y = 0;

  private yesterday_func_diff_pct_x = 0;
  private yesterday_func_diff_pct_y = 0;
  private yesterday_nonfunc_diff_pct_x = 0;
  private yesterday_nonfunc_diff_pct_y = 0;
  private yesterday_start_time = 0;

  private week_func_diff_pct_x = 0;
  private week_func_diff_pct_y = 0;
  private week_nonfunc_diff_pct_x = 0;
  private week_nonfunc_diff_pct_y = 0;
  private week_start_time = 0;

  // List of flagged dipoles
  private anomalous_dipoles: DipoleRow[] = [];
  private filtered_dipoles: DipoleRow[] = [];
  private filtered: boolean = false;
  private has_test: boolean = false;

  private test_data: Test;
  private start_time_string: string;
  private test_yesterday: Test;
  private week_tests: Test[] = [];
  private dipoles_data: Dipole[] = [];
  private tile_data: Tile;
  private anomalies: Anomaly[] = [];

  private obs_id_list: number[] = [];
  private yesterday_obs_id_list: number[] = [];
  private week_obs_list: number[] = [];
  private anomaly_type_list: AnomalyType[] = [];
  private filtered_obs: Observation[] = [];
  private anomaly_type_id_list: number[] = [];

  private readonly months: string[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  private readonly dipole_names: string[] = ["AllOn", "AllOff", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "AllOff", "AllOn"];

  // Constants in UTC Seconds
  private readonly GPS_START = 315964782;
  private readonly DAY_IN_SECONDS = 86400;
  private readonly WEEK_IN_SECONDS = 604800;

  constructor(
    private routingService: RoutingService,
    private testService: TestService,
    private anomalyService: AnomalyService,
    private anomalyTypeService: AnomalyTypeService,
    private dipoleService: DipoleService,
    private tileService: TileService) {

    this.test_id = Number(HealthReportsComponent.testid);
    console.log("Getting Test with ID = " + this.test_id);

    // Get the data from testService
    this.testService.getTestWithObservations(this.test_id)
      .do(data => {
        this.test_data = data;

        // Convert start_time to Date/Time
        let start_time = new Date(0);
        start_time.setUTCSeconds(this.test_data.start_time + this.GPS_START);

        this.start_time_string =
          this.months[start_time.getMonth()] +
          ' ' + start_time.getDate() +
          ', ' + start_time.getFullYear() +
          ' ' + start_time.getHours() +
          ':' + start_time.getMinutes() +
          ':' + start_time.getSeconds();

        for(let obs of this.test_data.observations) {
          this.obs_id_list.push(obs.id);
        }
      })
      .switchMap(() => this.tileService.getTileById(this.test_data.tile_id))
      .do(data => {
        this.tile_data = data;
        this.has_test = true;
      })
      // Get dipoles using test's tile
      .switchMap(() => this.dipoleService.getDipoles(this.test_data.tile_id))
      .do(data => {
        this.dipoles_data = data;
      })
      // Get anomalies using test's observation ids
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.obs_id_list))
      .do(data => {
        this.anomalies = data;

        if(this.anomalies.length == 0) {
          alert('This test does not contain any flagged dipoles')
        }

        // Put into a list for get request
        for(let anom of this.anomalies) {
          this.anomaly_type_id_list.push(anom.type);
        }

        // We only care about observations that correspond to an anomaly
        this.filtered_obs = this.test_data.observations.filter((obs) => {
          for(let anom of this.anomalies) {
            if(obs.id == anom.observation) {
              return true;
            }
          }
        });
      })
      // Get anomaly types using anomaly type id list
      .switchMap(() => this.anomalyTypeService.getAnomalyTypeByIdList(this.anomaly_type_id_list))
      .do(data => {
        this.anomaly_type_list = data;

        // Iterate through filtered observations to find anomalous dipoles
        for(let obs of this.filtered_obs) {
          let anomaly = this.anomalies.find((anomaly) => obs.id == anomaly.observation);
          let anomalyType = this.anomaly_type_list.find((anomalyType) => anomalyType.id == anomaly.type);

          let dipole = this.dipoles_data.find((dipole) => obs.dipole_id == dipole.id);

          let dipoleRow: DipoleRow = new DipoleRow(this.tile_data.name, this.dipole_names[dipole.dipole_number], dipole.polarity, anomalyType.name);

          this.filtered_dipoles.push(dipoleRow);
          this.anomalous_dipoles.push(dipoleRow);

          this.anomalous_dipoles.sort((dipole1, dipole2) => {
            if(dipole1.getAnomaly() < dipole2.getAnomaly()) {
              return -1;
            } else if(dipole1.getAnomaly() > dipole2.getAnomaly()) {
              return 1;
            } else {
              return 0;
            }
          });
        }

        // Count anomalies for each polarity
        for(let dipole of this.anomalous_dipoles) {
          if(dipole.getPolarity() == 'X') {
            this.anomaly_counter_x++;
          } else if(dipole.getPolarity() == 'Y') {
            this.anomaly_counter_y++;
          }
        }

        // Get all tests from yesterday midnight
        let start_time: Date = new Date(0);
        start_time.setUTCSeconds(this.test_data.start_time - this.DAY_IN_SECONDS);
        start_time.setHours(0);
        start_time.setMinutes(0);
        start_time.setSeconds(0);

        this.yesterday_start_time = start_time.getTime() / 1000;
      })
      .switchMap(() => this.testService.getTestsByDateRangeWithObservations(this.yesterday_start_time, this.test_data.start_time - 1))
      .do(data => {
        // Find yesterday's test of this tile
        this.test_yesterday = data.find((test) => this.test_data.tile_id == test.tile_id && this.test_data.start_time != test.start_time);

        // Calculate percentage of current non functional dipoles per polarity
        this.current_nonfunc_pct_x = (this.anomaly_counter_x/this.total_dipole) * 100;
        this.current_nonfunc_pct_y = (this.anomaly_counter_y/this.total_dipole) * 100;

        // Check if there is a test for this tile yesterday
        if (this.test_yesterday) {
          for (let obs of this.test_yesterday.observations) {
            this.yesterday_obs_id_list.push(obs.id);
          }
        } else {
          console.log("No Tests for this tile yesterday");
        }
      })
      // Get anomalies from yesterday's test
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.yesterday_obs_id_list))
      .do(data => {
        if(this.test_yesterday) {
          // Filter observations to only ones in anomalies from the request
          let filtered_obs = this.test_yesterday.observations.filter((obs) => {
            for (let anom of data) {
              return anom.observation == obs.id;
            }
          });

          let yesterday_anomaly_counter_x = 0;
          let yesterday_anomaly_counter_y = 0;

          let yesterday_nonfunc_pct_x = 0;
          let yesterday_nonfunc_pct_y = 0;
          let yesterday_func_pct_x = 0;
          let yesterday_func_pct_y = 0;

          // Calculate yesterday's anomaly count per polarity
          for (let obs of filtered_obs) {
            let dipole = this.dipoles_data.find((dipole) => dipole.id == obs.dipole_id);

            if (dipole.polarity == 0) {
              yesterday_anomaly_counter_x++;
            } else if (dipole.polarity == 1) {
              yesterday_anomaly_counter_y++;
            }
          }

          // ------------------ START NON FUNCTIONAL CALCULATION ------------------ //
          // Calculate yesterday's non functional dipoles per polarity
          yesterday_nonfunc_pct_x = (yesterday_anomaly_counter_x / this.total_dipole) * 100;
          yesterday_nonfunc_pct_y = (yesterday_anomaly_counter_y / this.total_dipole) * 100;

          // Calculate the percentage difference between today and yesterday
          this.yesterday_nonfunc_diff_pct_x = this.current_nonfunc_pct_x - yesterday_nonfunc_pct_x;
          this.yesterday_nonfunc_diff_pct_y = this.current_nonfunc_pct_y - yesterday_nonfunc_pct_y;

          // X Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.yesterday_nonfunc_diff_pct_x > 0) {
            // Create new span to edit default to point up
            let new_dropup = document.createElement('span');
            new_dropup.setAttribute("class", "dropup");
            new_dropup.setAttribute("id", "nonfunc-x-new-dropup");
            new_dropup.setAttribute("color", "green");

            // Change color of elements to green and append new span
            document.getElementById("nonfunc-x").style.setProperty("color", "green");
            document.getElementById("nonfunc-x").insertBefore(new_dropup, document.getElementById("nonfunc-x-caret"));
            new_dropup.appendChild(document.getElementById("nonfunc-x-caret"));
            document.getElementById("nonfunc-x-caret").style.setProperty("color", "green");
          } else if (this.yesterday_nonfunc_diff_pct_x == 0) {
            document.getElementById("nonfunc-x").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-y-caret").classList.add("caret-left-counter");
            document.getElementById("func-y-caret").classList.remove("caret");
            document.getElementById("nonfunc-x-caret").style.setProperty("color", "#5a5a5a");
          }

          // Y Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.yesterday_nonfunc_diff_pct_y > 0) {
            // Create new span to edit default to point up
            let new_dropup = document.createElement('span');
            new_dropup.setAttribute("class", "dropup");
            new_dropup.setAttribute("id", "func-y-new-dropup");
            new_dropup.setAttribute("color", "green");

            // Change color of elements to green and append new span
            document.getElementById("nonfunc-y").style.setProperty("color", "green");
            document.getElementById("nonfunc-y").insertBefore(new_dropup, document.getElementById("nonfunc-y-caret"));
            new_dropup.appendChild(document.getElementById("nonfunc-y-caret"));
            document.getElementById("nonfunc-y-caret").style.setProperty("color", "green");
          } else if (this.yesterday_nonfunc_diff_pct_y == 0) {
            document.getElementById("nonfunc-y").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-y-caret").classList.add("caret-left-counter");
            document.getElementById("func-y-caret").classList.remove("caret");
            document.getElementById("nonfunc-y-caret").style.setProperty("color", "#5a5a5a");
          }
          // ------------------ END NON FUNCTIONAL CALCULATION ------------------ //

          // ------------------ START FUNCTIONAL CALCULATION ------------------ //
          // Calculate yesterday's functional dipoles per polarity
          let yesterday_func_dipole_x = this.total_dipole - yesterday_anomaly_counter_x;
          let yesterday_func_dipole_y = this.total_dipole - yesterday_anomaly_counter_y;

          // Calculate current functional dipoles percentage per polarity
          this.current_func_pct_x = ((this.total_dipole - this.anomaly_counter_x) / this.total_dipole) * 100;
          this.current_func_pct_y = ((this.total_dipole - this.anomaly_counter_y) / this.total_dipole) * 100;

          // Calculate yesterday's functional dipoles percentage per polarity
          yesterday_func_pct_x = (yesterday_func_dipole_x / this.total_dipole) * 100;
          yesterday_func_pct_y = (yesterday_func_dipole_y / this.total_dipole) * 100;

          // Calculate the percentage difference between today and yesterday
          this.yesterday_func_diff_pct_x = this.current_func_pct_x - yesterday_func_pct_x;
          this.yesterday_func_diff_pct_y = this.current_func_pct_y - yesterday_func_pct_y;

          // X Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.yesterday_func_diff_pct_x > 0) {
            // Create new span to edit default to point up
            let new_dropup = document.createElement('span');
            new_dropup.setAttribute("class", "dropup");
            new_dropup.setAttribute("id", "func-x-new-dropup");
            new_dropup.setAttribute("color", "green");

            // Change color of elements to green and append new span
            document.getElementById("func-x").style.setProperty("color", "green");
            document.getElementById("func-x").insertBefore(new_dropup, document.getElementById("func-x-caret"));
            new_dropup.appendChild(document.getElementById("func-x-caret"));
            document.getElementById("func-x-caret").style.setProperty("color", "green");
          } else if (this.yesterday_func_diff_pct_x == 0) {
            document.getElementById("func-x").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-y-caret").classList.add("caret-left-counter");
            document.getElementById("func-y-caret").classList.remove("caret");
            document.getElementById("func-x-caret").style.setProperty("color", "#5a5a5a");
          }

          // Y Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.yesterday_func_diff_pct_y > 0) {
            // Create new span to edit default to point up
            let new_dropup = document.createElement('span');
            new_dropup.setAttribute("class", "dropup");
            new_dropup.setAttribute("id", "func-y-new-dropup");
            new_dropup.setAttribute("color", "green");

            // Change color of elements to green and append new span
            document.getElementById("func-y").style.setProperty("color", "green");
            document.getElementById("func-y").insertBefore(new_dropup, document.getElementById("func-y-caret"));
            new_dropup.appendChild(document.getElementById("func-y-caret"));
            document.getElementById("func-y-caret").style.setProperty("color", "green");
          } else if (this.yesterday_func_diff_pct_y == 0) {
            document.getElementById("func-y").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-y-caret").classList.add("caret-left-counter");
            document.getElementById("func-y-caret").classList.remove("caret");
            document.getElementById("func-y-caret").style.setProperty("color", "#5a5a5a");
          }
          // ------------------ END FUNCTIONAL CALCULATION ------------------ //
        }

        // Get all tests from midnight last week
        let start_time: Date = new Date(0);
        start_time.setUTCSeconds(this.test_data.start_time - this.WEEK_IN_SECONDS);
        start_time.setHours(0);
        start_time.setMinutes(0);
        start_time.setSeconds(0);

        this.week_start_time = start_time.getTime() / 1000;
      })
      .switchMap(() => this.testService.getTestsByDateRangeWithObservations(this.week_start_time, this.test_data.start_time - 1))
      .do(data => {
        this.week_tests = data.filter((test) => this.test_data.tile_id == test.tile_id && this.test_data.start_time != test.start_time);

        if(this.week_tests.length > 0) {
          for (let test of this.week_tests) {
            for (let obs of test.observations) {
              this.week_obs_list.push(obs.id);
            }
          }
        } else {
          console.log("No tests for this tile from last week!")
        }
      })
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.week_obs_list))
      .subscribe(data => {
        if(this.week_tests.length > 0) {
          let anomaly_counter_x = 0;
          let anomaly_counter_y = 0;

          let filtered_obs = [];
          // Count all anomalies from previous week
          for(let test of this.week_tests) {
            filtered_obs = test.observations.filter((obs) => {
              for(let anom of data) {
                return obs.id == anom.observation
              }
            });

            for(let obs of filtered_obs) {
              let dipole = this.dipoles_data.find((dipole) => obs.dipole_id == dipole.id);

              if(dipole.polarity == 0) {
                anomaly_counter_x++;
              } else if(dipole.polarity == 1) {
                anomaly_counter_y++;
              }
            }
          }
          // ------------------ START NON FUNCTIONAL CALCULATION ------------------ //
          let week_nonfunc_pct_x = (anomaly_counter_x / (this.total_dipole * this.week_tests.length)) * 100;
          let week_nonfunc_pct_y = (anomaly_counter_y / (this.total_dipole * this.week_tests.length)) * 100;

          this.week_nonfunc_diff_pct_x = this.current_nonfunc_pct_x - week_nonfunc_pct_x;
          this.week_nonfunc_diff_pct_y = this.current_nonfunc_pct_y - week_nonfunc_pct_y;

          // X Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.week_nonfunc_diff_pct_x < 0) {
            // Append dropup so it turns to down arrow
            let table_column = document.getElementById("nonfunc-x");
            table_column.insertBefore(document.getElementById("nonfunc-x-dropup-caret"), document.getElementById("nonfunc-x-dropup"));
            document.getElementById("nonfunc-x-dropup").style.setProperty("color", "red");
            document.getElementById("nonfunc-x-dropup-caret").style.setProperty("color", "red");
          } else if (this.week_nonfunc_diff_pct_x == 0) {
            document.getElementById("nonfunc-x-dropup").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-y-caret").classList.add("caret-left-counter");
            document.getElementById("func-y-caret").classList.remove("caret");
            document.getElementById("nonfunc-x-dropup-caret").style.setProperty("color", "#5a5a5a");
          }

          // Y Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.week_nonfunc_diff_pct_y < 0) {
            // Append dropup so it turns to down arrow
            let table_column = document.getElementById("nonfunc-y");
            table_column.insertBefore(document.getElementById("nonfunc-y-dropup-caret"), document.getElementById("nonfunc-y-dropup"));
            document.getElementById("nonfunc-y-dropup").style.setProperty("color", "red");
            document.getElementById("nonfunc-y-dropup-caret").style.setProperty("color", "red");
          } else if (this.week_nonfunc_diff_pct_y == 0) {
            document.getElementById("nonfunc-y-dropup").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-y-caret").classList.add("caret-left-counter");
            document.getElementById("func-y-caret").classList.remove("caret");
            document.getElementById("nonfunc-y-dropup-caret").style.setProperty("color", "#5a5a5a");
          }
          // ------------------ END NON FUNCTIONAL CALCULATION ------------------ //

          // ------------------ START FUNCTIONAL CALCULATION ------------------ //
          let week_func_pct_x = (((this.total_dipole * this.week_tests.length) - anomaly_counter_x) / (this.total_dipole * this.week_tests.length)) * 100;
          let week_func_pct_y = (((this.total_dipole * this.week_tests.length) - anomaly_counter_y) / (this.total_dipole * this.week_tests.length)) * 100;

          this.week_func_diff_pct_x = this.current_func_pct_x - week_func_pct_x;
          this.week_func_diff_pct_y = this.current_func_pct_y - week_func_pct_y;

          // X Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.week_func_diff_pct_x < 0) {
            // Append dropup so it turns to down arrow
            let table_column = document.getElementById("func-x");
            table_column.insertBefore(document.getElementById("func-x-dropup-caret"), document.getElementById("func-x-dropup"));
            document.getElementById("func-x-dropup").style.setProperty("color", "red");
            document.getElementById("func-x-dropup-caret").style.setProperty("color", "red");
          } else if (this.week_func_diff_pct_x == 0) {
            document.getElementById("func-x-dropup").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-x-dropup-caret").style.setProperty("class", "caret-left-counter");
            document.getElementById("func-x-dropup-caret").style.setProperty("color", "#5a5a5a");
          }

          // Y Polarity
          // Change color of html elements (default color is red for negative diff)
          if (this.week_func_diff_pct_y < 0) {
            // Append dropup so it turns to down arrow
            let table_column = document.getElementById("func-y");
            table_column.insertBefore(document.getElementById("func-y-dropup-caret"), document.getElementById("func-y-dropup"));
            document.getElementById("func-y-dropup").style.setProperty("color", "red");
            document.getElementById("func-y-dropup-caret").style.setProperty("color", "red");
          } else if (this.week_func_diff_pct_y == 0) {
            document.getElementById("func-y-dropup").style.setProperty("color", "#5a5a5a");
            document.getElementById("func-y-dropup-caret").style.setProperty("class", "caret-left-counter");
            document.getElementById("func-y-dropup-caret").style.setProperty("color", "#5a5a5a");
          }
          // ------------------ END FUNCTIONAL CALCULATION ------------------ //
        }
      })
  }

  ngOnInit(): void {
  }

  routeTo(route: string): void {
    this.routingService.routeTo(route);
  }

  maximiseTable() {
    let maximised = document.getElementById("malfunc-table-div").style.getPropertyValue("height") == "98%";

    if (!maximised) {
      if(this.filtered) {
        this.filtered = false;
      }
      document.getElementById("malfunc-table-div").style.setProperty("top", "2%");
      document.getElementById("malfunc-table-div").style.setProperty("height", "98%");
    } else {
        document.getElementById("malfunc-table-div").style.setProperty("top", "35%");
        document.getElementById("malfunc-table-div").style.setProperty("height", "63%");
    }
  }

  showFilterContainer() {
    if(document.getElementById("filter-container").style.getPropertyValue("visibility") == "hidden") {
      document.getElementById("filter-container").style.setProperty("visibility", "visible");
    } else {
      document.getElementById("filter-container").style.setProperty("visibility", "hidden");
    }
  }

  filterByAnomalyType() {
    // Hide div
    document.getElementById("filter-container").style.setProperty("visibility", "hidden");

    let anomaly_type_input = (<HTMLInputElement>document.getElementById("filter-input")).value;

    if(anomaly_type_input) {
      this.filtered = true;
      this.filtered_dipoles = this.anomalous_dipoles.filter((dipole) => dipole.getAnomaly().toLowerCase() == anomaly_type_input.toLowerCase());
    } else if(anomaly_type_input == "") {
      this.filtered = false;
    }
  }
}

// Container class for sorting purposes
class DipoleRow {
  private tileNum: string;
  private dipoleID: string;
  private polarity: string;
  private anomaly: string;

  constructor(tileNum: string, dipoleID: string, polarity: number, anomaly: string) {
    this.tileNum = tileNum;
    this.dipoleID = dipoleID;
    this.polarity = (polarity == 0) ? 'X' : 'Y';
    this.anomaly = anomaly;
  }

  getTileNum(): string {
    return this.tileNum;
  }

  getDipoleId(): string {
    return this.dipoleID;
  }

  getPolarity(): string {
    return this.polarity;
  }

  getAnomaly(): string {
    return this.anomaly;
  }
}