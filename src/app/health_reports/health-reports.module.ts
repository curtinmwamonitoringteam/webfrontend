import { NgModule } from '@angular/core';
import { HealthReportsComponent } from './health-reports.component';
import { HealthReportsDropdownComponent } from './dropdown.component'
import { HealthReportsSingularComponent } from "./health-report-singular.component";
import { BrowserModule } from "@angular/platform-browser";
import { RibbonModule } from '../ribbon/ribbon.module';
import { TestService } from '../repository/test.service';
import { AnomalyService } from "../repository/anomaly.service";
import { AnomalyTypeService } from "../repository/anomalyType.service";
import { DipoleService } from "../repository/dipole.service";
import {ObservationService} from "../repository/observation.service";


@NgModule({
  imports: [
    BrowserModule,
    RibbonModule,
  ],
  declarations: [
    HealthReportsComponent,
    HealthReportsDropdownComponent,
    HealthReportsSingularComponent,
  ],
  exports: [
    HealthReportsComponent,
  ],

  providers: [
    TestService,
    AnomalyService,
    AnomalyTypeService,
    DipoleService,
    ObservationService,
  ]
})
export class HealthReportModule {

}