import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HealthReportsComponent, TestRow} from './health-reports.component';
import {HealthReportsDropdownComponent} from "./dropdown.component";
import {RibbonComponent} from "../ribbon/ribbon.component";
import {TestService} from "../repository/test.service";
import {httpFactory, HttpInterceptor} from "../interceptor/http-interceptor";
import {ConnectionBackend, HttpModule, RequestOptions, XHRBackend} from "@angular/http";
import {RibbonModule} from "../ribbon/ribbon.module";
import {AnomalyService} from "../repository/anomaly.service";
import {ReflectiveInjector} from "@angular/core";
import {RoutingService} from "../navigation/routing.service";

describe('Health Reports Component', () => {
  let component: HealthReportsComponent;
  let fixture: ComponentFixture<HealthReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        RibbonModule,
      ],
      declarations: [
        HealthReportsComponent,
        HealthReportsDropdownComponent,
        RibbonComponent,
      ],
      providers: [
        TestService,
        AnomalyService,
        HttpInterceptor,
        ConnectionBackend,
        RoutingService,
        {provide: HttpInterceptor, userFactory: httpFactory, deps: [XHRBackend, RequestOptions]}
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(HealthReportsComponent);
    // component test instance
    component = fixture.componentInstance;

    let anomalous_dipoles: {dipole: string, polarity: string}[] = [];
    let test: TestRow = new TestRow(1, "1", "12/12/2017", anomalous_dipoles);

    component.sorted_testlist.push(test);

    fixture.detectChanges();
  }));

  it('should create right component', () => {
    fixture.detectChanges();
    expect(component instanceof HealthReportsComponent)
      .toBe(true, 'should create HealthReportsComponent');
  });

  it('should contain tests', () => {
    let anomalous_dipoles: {dipole: string, polarity: string}[] = [];
    let test: TestRow = new TestRow(1, "1", "12/12/2017", anomalous_dipoles);
    fixture.detectChanges();
    expect(component.sorted_testlist).toContain(test, "doesnt contain test");
    expect(component.getTestList().length)
      .toBeGreaterThan(0, 'should contain data in list');
  });

  it('shouldn\'t contain tests', () => {
    component.sorted_testlist = [];
    fixture.detectChanges();
    expect(component.getTestList().length)
      .toBeLessThanOrEqual(0, 'shouldn\'t contain data in list');
  });
});