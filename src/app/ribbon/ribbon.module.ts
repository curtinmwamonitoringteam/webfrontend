
import { NgModule } from '@angular/core';
import { RibbonComponent } from './ribbon.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    RibbonComponent,
  ],
  imports: [
    BrowserModule,
  ],
  exports: [
    RibbonComponent,
  ]
})
export class RibbonModule{
}