import { Component, Input, OnInit } from '@angular/core';
import { RibbonGroup, RibbonGroupType } from './ribbon-group.class';

@Component({
  selector: 'mwa-ribbon',
  templateUrl: './ribbon.component.html',
  styleUrls: ['./ribbon.component.css']
})
export class RibbonComponent implements OnInit {

  @Input() groupList: RibbonGroup[];
  grpType = RibbonGroupType;
  collapse = false;

  constructor() {
  }

  ngOnInit() {
    let itemWidth = 0;
    for (const item of this.groupList) {
      itemWidth += item.width;
    }
    if (itemWidth >= window.innerWidth) {
      this.collapse = true;
    }
  }

}
