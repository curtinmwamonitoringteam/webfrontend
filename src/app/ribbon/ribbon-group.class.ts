export class RibbonGroup {

  title: string;
  type: RibbonGroupType;
  width: number;
  private _items: any[];


  constructor(title: string, type: RibbonGroupType, width: number, items: any[]) {
    if (width > 0) {
      this.width = width;
    }
    this.items = items;
    this.type = type;
    this.title = title;
  }


  public get items(): any[] {
    return this._items;
  }

  public set items(value: any[]) {
    this._items = value;
  }

  public addItem(value: any) {
    this._items.push(value);
  }

  public removeItem(value: any) {
    const index = this._items.indexOf(value, 0);
    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

}

export enum RibbonGroupType {LABEL, BUTTON, CHECKBOX}

