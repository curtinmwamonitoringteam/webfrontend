import { NgModule } from '@angular/core';
import { StatisticsComponent } from './statistics.component';
import { StatisticsTileDetailsComponent } from "./statistics-tile-details.component";
import { BrowserModule } from "@angular/platform-browser";
import { RibbonModule } from '../ribbon/ribbon.module';
import { TestService } from "../repository/test.service";
import { AnomalyService } from "../repository/anomaly.service";
import { TileService } from "../repository/tile.service";
import { AnomalyTypeService } from "../repository/anomalyType.service";
import {StatisticsGraphComponent} from "./statistics-graph.component";
import {ChartsModule} from "ng2-charts";

@NgModule({
  imports: [
    BrowserModule,
    RibbonModule,
    ChartsModule,
  ],
  declarations: [
    StatisticsComponent,
    StatisticsTileDetailsComponent,
    StatisticsGraphComponent,
  ],
  exports: [
    StatisticsComponent,
  ],
  providers: [
    TestService,
    AnomalyService,
    TileService,
    AnomalyTypeService,
  ]

})
export class StatisticsModule {

}
