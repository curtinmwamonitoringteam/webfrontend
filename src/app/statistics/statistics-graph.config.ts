
export let chart_options = {
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  title: {
    display: true,
    text: 'Number of Anomalous Readings by Date',
  },
  elements: {
    line: {
      tension: 0,
      borderWidth: 0.5,
      borderColor: 'rgba(255, 255, 255, 1.0)',
      fill: false,
      cubicInterpolationMode: 'monotone',
    },
    point: {
      radius: 0,
      hoverRadius: 8,
      hitRadius: 4,
      borderColor: 'rgba(200, 200, 200, 0.5)',
    },
  },
  scales: {
    yAxes: [
      {
        display: true,
        ticks: {
          min: 0,
          max: 30,
        }
      }
    ],
  },
};