import {Component, Input} from "@angular/core";
import {chart_options} from "./statistics-graph.config";
import {StatisticsComponent} from "./statistics.component";
import {Test} from "../model/test.class";
import {Anomaly} from "../model/anomaly.class";
import {TestService} from "../repository/test.service";
import {AnomalyService} from "../repository/anomaly.service";

@Component({
  selector: 'mwa-statistics-graph',
  templateUrl: 'statistics-graph.component.html',
  styleUrls: ['statistics-graph.component.css']
})
export class StatisticsGraphComponent {
  private readonly GPS_START = 315964782;

  private test_list: Test[] = [];
  private anomalies_list: Anomaly[] = [];
  private date_map: Map<string, number> = new Map<string, number>();
  private all_tiles: boolean = true;

  // chart data
  chart_data: number[] = [];
  chart_label: string[] = [];
  chart_type = 'line';
  chart_options = JSON.parse(JSON.stringify(chart_options));

  private hasData: boolean = false;
  private observation_id_list: number[] = [];

  constructor(private testService: TestService, private anomalyService: AnomalyService) {
    let start_date = new Date(0);
    start_date.setUTCSeconds(StatisticsComponent.start_GPS + this.GPS_START);
    let end_date = new Date(0);
    end_date.setUTCSeconds(StatisticsComponent.end_GPS + this.GPS_START);
    this.all_tiles = StatisticsComponent.spec_tile_num == 0;
    if(this.all_tiles) {
      this.getAllTiles();
    } else {
      this.getSpecificTile();
    }
  }

  getSpecificTile() {
    this.chart_data = [];
    this.chart_label = [];
    this.testService.getTestsByDateRangeWithObservations(StatisticsComponent.start_GPS, StatisticsComponent.end_GPS)
      .do((data) => {
        this.test_list = data;

        for(let test of data) {
          for(let obs of test.observations) {
            this.observation_id_list.push(obs.id);
          }
        }
      })
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.observation_id_list))
      .subscribe((data) => {
        this.anomalies_list = data;

        for(let test of this.test_list) {
          // Filter anomalies for each test
          let filtered_anomalies = this.anomalies_list.filter((anom) => {
            for (let obs of test.observations) {
              if(obs.id == anom.observation) {
                return true;
              }
            }
          });

          if(filtered_anomalies.length > 0) {
            if(test.tile_id == StatisticsComponent.spec_tile_num) {
              let date = new Date(0);
              date.setUTCSeconds(test.start_time + this.GPS_START);
              let label_string = date.getDate() + "/" + (date.getMonth() + 1).toString() + "/" + date.getFullYear();
              this.chart_label.unshift(label_string);
              this.chart_data.unshift(filtered_anomalies.length)
            }
          }
        }

        console.log(this.chart_label);
        console.log(this.chart_data);
        this.hasData = true;
        this.all_tiles = false;
      })
  }

  getAllTiles() {
    this.chart_data = [];
    this.chart_label = [];
    this.testService.getTestsByDateRangeWithObservations(StatisticsComponent.start_GPS, StatisticsComponent.end_GPS)
      .do((data) => {
        this.test_list = data;

        for(let test of data) {
          for(let obs of test.observations) {
            this.observation_id_list.push(obs.id);
          }
        }
      })
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.observation_id_list))
      .subscribe((data) => {
        this.anomalies_list = data;

        let data_chart = [];

        for(let test of this.test_list) {
          // Filter anomalies for each test
          let filtered_anomalies = this.anomalies_list.filter((anom) => {
            for (let obs of test.observations) {
              if(obs.id == anom.observation) {
                return true;
              }
            }
          });

          if(filtered_anomalies.length > 0) {
            let date = new Date(0);
            date.setUTCSeconds(test.start_time + this.GPS_START);
            let label_string = date.getDate() + "/" + (date.getMonth() + 1).toString() + "/" + date.getFullYear();

            if (this.date_map.has(label_string)) {
              this.date_map.set(label_string, this.date_map.get(label_string) + filtered_anomalies.length);
            } else {
              this.date_map.set(label_string, filtered_anomalies.length);
            }
          }
        }

        this.date_map.forEach((value, key, map) => {
          this.chart_label.unshift(key);
          this.chart_data.unshift(value);
        });

        console.log(this.chart_label);
        console.log(this.chart_data);
        this.hasData = true;
        this.all_tiles = true;
      })
  }

  ngOnInit() {
  }
}