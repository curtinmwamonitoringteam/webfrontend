import { Component } from '@angular/core';
import { RibbonGroup, RibbonGroupType } from '../ribbon/ribbon-group.class';
import { TestService } from "../repository/test.service";
import { AnomalyTypeService } from "../repository/anomalyType.service";
import { AnomalyService } from "../repository/anomaly.service";
import { Anomaly } from "../model/anomaly.class";
import { AnomalyType } from "../model/anomaly-type.class";
import { Test } from "../model/test.class";
import { RoutingService } from "../navigation/routing.service";
import {TileService} from "../repository/tile.service";
import {Tile} from "../model/tile.class";

@Component({
  selector: 'mwa-statistics',
  templateUrl: 'statistics.component.html',
  styleUrls: ['statistics.component.css'],
})

export class StatisticsComponent {
  static spec_tile_num: number = 0;
  static spec_tile_name: string = "";
  static spec_malfunc_counter: number = 0;
  static spec_anom_counter: number = 0;
  static spec_start_GPS: number = 0;
  static spec_end_GPS: number = 0;
  static start_GPS: number = 0;
  static end_GPS: number = 0;

  ribbonData: RibbonGroup[];
  private tiles_list: { tile_number: number, tile_name: string, anomalous_dipoles: number, malfunctioning_dipoles: number}[] = [];
  private occurrence_list: { anomaly_type: string, occurrence: number, occurrence_rate: string}[] = [];
  private tiles_map: Map<string, number[]> = new Map<string, number[]>();

  private test_list: Test[] = [];
  private tiles: Tile[] = [];
  private anomalies_list: Anomaly[] = [];
  private anomaly_type_list: AnomalyType[] = [];
  private observation_id_list: number[] = [];

  private bad_dipoles_counter = 0;

  // Counters for anomaly type occurrence
  private re_counter = 0;
  private pwo_counter = 0;
  private cl_counter = 0;
  private fc_counter = 0;
  private ab_counter = 0;
  private od_counter = 0;
  private dtp_counter = 0;
  private lhp_counter = 0;
  private rfp_counter = 0;
  private ddh_counter = 0;
  private dd_counter = 0;

  // GPS Time start in UTC seconds
  private readonly GPS_START = 315964782;
  private readonly MONTH_IN_SECONDS = 2629743;

  private start_date_GPS = (Math.trunc(Date.now()/1000) - this.GPS_START) - (this.MONTH_IN_SECONDS * 2);
  private end_date_GPS = Math.trunc(Date.now()/1000) - this.GPS_START;

  constructor(private routingService: RoutingService,
              private testService: TestService,
              private anomalyTypeService: AnomalyTypeService,
              private anomalyService: AnomalyService,
              private tileService: TileService) {
    StatisticsComponent.start_GPS = this.start_date_GPS;
    StatisticsComponent.end_GPS = this.end_date_GPS;
    this.fillDetails(this.start_date_GPS, this.end_date_GPS);
  }

  ngOnInit(): void {
    this.ribbonData = [
      new RibbonGroup('Filter Options', RibbonGroupType.LABEL, 105, [
        {label: 'Filter Options:'}
      ]),
      new RibbonGroup('Filters', RibbonGroupType.BUTTON, 400, [
        {label: 'Date Range', func: this.showDateRange}
      ]),
    ]
  }

  viewAll() {
    if(document.getElementById("tiles-flagged-container").style.getPropertyValue("height") != "98%") {
      document.getElementById("tiles-flagged-container").style.setProperty("top", "2%");
      document.getElementById("tiles-flagged-container").style.setProperty("height", "98%");
      document.getElementById("tiles-flagged-container").style.setProperty("z-index", "2");
    } else {
      document.getElementById("tiles-flagged-container").style.setProperty("top", "28%");
      document.getElementById("tiles-flagged-container").style.setProperty("height", "30%");
    }
  }

  showDateRange() {
    if(document.getElementById("date-range-container").style.getPropertyValue("visibility") == "hidden") {
      document.getElementById("date-range-container").style.setProperty("visibility", "visible");
    } else {
      document.getElementById("date-range-container").style.setProperty("visibility", "hidden");
    }
  }

  filterByDateRange() {
    // Get values of date input from page
    let start_date = new Date((<HTMLInputElement>document.getElementById("date-input-start")).value);
    let end_date =  new Date((<HTMLInputElement>document.getElementById("date-input-end")).value);

    if(start_date <= end_date) {
      // Hide date range div on press of confirm
      document.getElementById("date-range-container").style.setProperty("visibility", "hidden");

      // Convert dates to GPS time
      this.start_date_GPS = start_date.getTime()/1000 - this.GPS_START;
      this.end_date_GPS = end_date.getTime()/1000 - this.GPS_START;

      // Clear everything on page
      this.bad_dipoles_counter = 0;
      this.tiles_map = new Map<string, number[]>();
      this.tiles_list = [];
      this.occurrence_list = [];
      this.anomalies_list = [];
      this.observation_id_list = [];
      this.anomaly_type_list = [];
      this.resetCounters();

      // Rebuild page using input
      this.fillDetails(this.start_date_GPS, this.end_date_GPS);
    }
  }

  fillDetails(start_date: number, end_date: number) {
    this.testService.getTestsByDateRangeWithObservations(start_date, end_date)
    .do((data) => {
      this.test_list = data;

      if(this.test_list.length == 0) {
        alert("No tests within the range!");
        this.observation_id_list.push(0);
        this.observation_id_list.push(-1);
      }

      for(let test of this.test_list) {
        for(let obs of test.observations) {
          this.observation_id_list.push(obs.id);
        }
      }
    })
    .switchMap(() => this.tileService.getAllTiles())
    .do(data => {
      this.tiles = data;
    })
    .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.observation_id_list))
    .do((data) => {
      this.anomalies_list = data;

      this.bad_dipoles_counter = this.anomalies_list.length;

      this.fillFlaggedTable();
    })
    .switchMap(() => this.anomalyTypeService.getAnomalyTypeAll())
    .subscribe((data) => {
      this.anomaly_type_list = data;

      // Counts the occurrence of each anomaly type
      this.countOccurrence();

      // Calculates the occurrence rate and displays it
      this.fillOccurrenceTable();
    })
  }

  fillFlaggedTable() {
    for(let test of this.test_list) {
      // Filter anomalies for each test
      let filtered_anomalies = this.anomalies_list.filter((anom) => {
        for(let obs of test.observations) {
          if(obs.id == anom.observation) {
            return true;
          }
        }
      });

      // Only add tests with anomalies
      if(filtered_anomalies.length > 0) {
        let malfunctioning_counter: number = 0;
        let anomalous_counter: number = 0;

        // Anomaly Type IDs 1,2,3,5,6 are categorised as malfunctioning
        for (let anomaly of filtered_anomalies) {
          switch (anomaly.type) {
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
              malfunctioning_counter++;
              break;
            default:
              anomalous_counter++;
              break;
          }
        }

        let tile = this.tiles.find((tile) => tile.id == test.tile_id);

        // Update flagged counts if already in map
        if(this.tiles_map.has(tile.name)) {
          this.tiles_map.set(tile.name,
            [
              this.tiles_map.get(tile.name)[0] + anomalous_counter,
              this.tiles_map.get(tile.name)[1] + malfunctioning_counter
            ]);
        } else {
          // Add new tiles to tiles map
          this.tiles_map.set(tile.name,
            [
              anomalous_counter,
              malfunctioning_counter
            ]);
        }
      }
    }

    // Transfer map to list for ngFor
    this.tiles_map.forEach((value, key, map) => {
      let tile = this.tiles.find((tile) => tile.name == key);

      this.tiles_list.push({
        tile_number: tile.id,
        tile_name: key,
        anomalous_dipoles: value[0],
        malfunctioning_dipoles: value[1]
      })
    });
  }

  countOccurrence() {
    let filtered_anom = this.anomalies_list.filter((anom) => {
      for(let obs of this.observation_id_list) {
        if(obs == anom.observation) {
          return true;
        }
      }
    });

    for(let anomaly of filtered_anom) {
      switch(anomaly.type) {
        case 1:
          // Rainbow Effect
          this.re_counter++;
          break;
        case 2:
          // Power While Off
          this.pwo_counter++;
          break;
        case 3:
          // Clustered Lines
          this.cl_counter++;
          break;
        case 4:
          // Flipped Cables
          this.fc_counter++;
          break;
        case 5:
          // All Black
          this.ab_counter++;
          break;
        case 6:
          // Off Dipole
          this.od_counter++;
          break;
        case 7:
          // Different Tile Polarization
          this.dtp_counter++;
          break;
        case 8:
          // Lower Historical Power
          this.lhp_counter++;
          break;
        case 9:
          // RF Pollution
          this.rfp_counter++;
          break;
        case 10:
          // Dipole Deviates Historically
          this.ddh_counter++;
          break;
        case 11:
          // Dipole Drifts
          this.dd_counter++;
          break;
      }
    }
  }

  fillOccurrenceTable() {
    let filtered_anom = this.anomalies_list.filter((anom) => {
      for(let obs of this.observation_id_list) {
        if(obs == anom.observation) {
          return true;
        }
      }
    });

    for(let anomaly_type of this.anomaly_type_list) {
      switch(anomaly_type.name) {
        case "Rainbow Effect":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.re_counter,
            occurrence_rate: ((this.re_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Power While Off":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.pwo_counter,
            occurrence_rate: ((this.pwo_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Clustered Lines":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.cl_counter,
            occurrence_rate: ((this.cl_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Flipped Cables":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.fc_counter,
            occurrence_rate: ((this.fc_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "All Black":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.ab_counter,
            occurrence_rate: ((this.ab_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Off Dipole":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.od_counter,
            occurrence_rate: ((this.od_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Different Tile Polarization":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.dtp_counter,
            occurrence_rate: ((this.dtp_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Lower Historical Power":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.lhp_counter,
            occurrence_rate: ((this.lhp_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "RF Pollution":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.rfp_counter,
            occurrence_rate: ((this.rfp_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Dipole Deviates Historically":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.ddh_counter,
            occurrence_rate: ((this.ddh_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Dipole Drifts":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.dd_counter,
            occurrence_rate: ((this.dd_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
      }
    }

    // Sort the table by occurrence
    this.occurrence_list.sort((anom1, anom2) => {
      if(anom1.occurrence < anom2.occurrence) {
        return 1;
      } else if(anom1.occurrence > anom2.occurrence) {
        return -1;
      } else {
        return 0;
      }
    })
  }

  resetCounters() {
    this.re_counter = 0;
    this.pwo_counter = 0;
    this.cl_counter = 0;
    this.fc_counter = 0;
    this.ab_counter = 0;
    this.od_counter = 0;
    this.dtp_counter = 0;
    this.lhp_counter = 0;
    this.rfp_counter = 0;
    this.ddh_counter = 0;
    this.dd_counter = 0;
  }

  saveTile(tile_num: number, tile_name: string, malfunc_counter: number, anom_counter: number, start_GPS: number, end_GPS: number) {
    StatisticsComponent.spec_tile_num = tile_num;
    StatisticsComponent.spec_tile_name = tile_name;
    StatisticsComponent.spec_malfunc_counter = malfunc_counter;
    StatisticsComponent.spec_anom_counter = anom_counter;
    StatisticsComponent.spec_start_GPS = start_GPS;
    StatisticsComponent.spec_end_GPS = end_GPS;
  }

  route() {
    this.routingService.routeTo("/statistics-tile-details");
  }
}
