import { Component } from '@angular/core';
import { RibbonGroup, RibbonGroupType } from '../ribbon/ribbon-group.class';
import {StatisticsComponent} from "./statistics.component";
import {TestService} from "../repository/test.service";
import {AnomalyTypeService} from "../repository/anomalyType.service";
import {AnomalyService} from "../repository/anomaly.service";
import {Test} from "../model/test.class";
import {Anomaly} from "../model/anomaly.class";
import {AnomalyType} from "../model/anomaly-type.class";

@Component({
  selector: 'mwa-statistics-tile-details',
  templateUrl: 'statistics-tile-details.component.html',
  styleUrls: ['statistics-tile-details.component.css'],
})

export class StatisticsTileDetailsComponent {
  ribbonData: RibbonGroup[];

  private test_list: Test[] = [];
  private anomalies_list: Anomaly[] = [];
  private occurrence_list: { anomaly_type: string, occurrence: number, occurrence_rate: string}[] = [];
  private anomaly_type_list: AnomalyType[] = [];
  private observation_id_list: number[] = [];

  private anomalous_counter = 0;
  private malfunc_counter = 0;

  // Counters for anomaly type occurrence
  private re_counter = 0;
  private pwo_counter = 0;
  private cl_counter = 0;
  private fc_counter = 0;
  private ab_counter = 0;
  private od_counter = 0;
  private dtp_counter = 0;
  private lhp_counter = 0;
  private rfp_counter = 0;
  private ddh_counter = 0;
  private dd_counter = 0;

  private tile_num: number = StatisticsComponent.spec_tile_num;
  private tile_name: string = StatisticsComponent.spec_tile_name;
  private start_GPS: number = StatisticsComponent.spec_start_GPS;
  private end_GPS: number = StatisticsComponent.spec_end_GPS;

  private start_date_string: string;
  private end_date_string: string;

  private hasTile: boolean = false;
  private readonly GPS_START: number = 315964782;
  private readonly months: string[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  constructor(private testService: TestService, private anomalyTypeService: AnomalyTypeService, private anomalyService: AnomalyService) {
    console.log("Displaying data for tile: " + StatisticsComponent.spec_tile_num);

    this.malfunc_counter = StatisticsComponent.spec_malfunc_counter;
    this.anomalous_counter = StatisticsComponent.spec_anom_counter;

    let start_date = new Date(0);
    start_date.setUTCSeconds(StatisticsComponent.spec_start_GPS + this.GPS_START);
    this.start_date_string =
      this.months[start_date.getMonth()] +
      " " + start_date.getDate() +
      ", " + start_date.getFullYear();

    let end_date = new Date(0);
    end_date.setUTCSeconds(StatisticsComponent.spec_end_GPS + this.GPS_START);
    this.end_date_string =
      this.months[end_date.getMonth()] +
      " " + end_date.getDate() +
      ", " + end_date.getFullYear();

    this.hasTile = true;

    this.testService.getTestsByDateRangeWithObservations(StatisticsComponent.spec_start_GPS, StatisticsComponent.spec_end_GPS)
      .do((data) => {
        this.test_list = data;

        for(let test of this.test_list) {
          if(test.tile_id == StatisticsComponent.spec_tile_num) {
            for(let obs of test.observations) {
              this.observation_id_list.push(obs.id);
            }
          }
        }
      })
      .switchMap(() => this.anomalyService.getAnomaliesFromObsIDs(this.observation_id_list))
      .do((data) => {
        this.anomalies_list = data;
      })
      .switchMap(() => this.anomalyTypeService.getAnomalyTypeAll())
      .subscribe((data) => {
        this.anomaly_type_list = data;

        // Counts the occurence of each anomaly type
        this.countOccurrence();

        // Calculates the occurence rate and displays it
        this.fillOccurrenceTable();
      })
  }


  ngOnInit(): void {
    this.ribbonData = [
      new RibbonGroup('Filter Options', RibbonGroupType.LABEL, 105, [
        {label: 'Filter Options:'}
      ]),
    ]
  }

  countOccurrence() {
    let filtered_anom = this.anomalies_list.filter((anom) => {
      for(let obs of this.observation_id_list) {
        if(obs == anom.observation) {
          return true;
        }
      }
    });

    for(let anomaly of filtered_anom) {
      switch(anomaly.type) {
        case 1:
          // Rainbow Effect
          this.re_counter++;
          break;
        case 2:
          // Power While Off
          this.pwo_counter++;
          break;
        case 3:
          // Clustered Lines
          this.cl_counter++;
          break;
        case 4:
          // Flipped Cables
          this.fc_counter++;
          break;
        case 5:
          // All Black
          this.ab_counter++;
          break;
        case 6:
          // Off Dipole
          this.od_counter++;
          break;
        case 7:
          // Different Tile Polarization
          this.dtp_counter++;
          break;
        case 8:
          // Lower Historical Power
          this.lhp_counter++;
          break;
        case 9:
          // RF Pollution
          this.rfp_counter++;
          break;
        case 10:
          // Dipole Deviates Historically
          this.ddh_counter++;
          break;
        case 11:
          // Dipole Drifts
          this.dd_counter++;
          break;
      }
    }
  }

  fillOccurrenceTable() {
    let filtered_anom = this.anomalies_list.filter((anom) => {
      for(let obs of this.observation_id_list) {
        if(obs == anom.observation) {
          return true;
        }
      }
    });

    for(let anomaly_type of this.anomaly_type_list) {
      switch(anomaly_type.name) {
        case "Rainbow Effect":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.re_counter,
            occurrence_rate: ((this.re_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Power While Off":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.pwo_counter,
            occurrence_rate: ((this.pwo_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Clustered Lines":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.cl_counter,
            occurrence_rate: ((this.cl_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Flipped Cables":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.fc_counter,
            occurrence_rate: ((this.fc_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "All Black":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.ab_counter,
            occurrence_rate: ((this.ab_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Off Dipole":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.od_counter,
            occurrence_rate: ((this.od_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Different Tile Polarization":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.dtp_counter,
            occurrence_rate: ((this.dtp_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Lower Historical Power":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.lhp_counter,
            occurrence_rate: ((this.lhp_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "RF Pollution":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.rfp_counter,
            occurrence_rate: ((this.rfp_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Dipole Deviates Historically":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.ddh_counter,
            occurrence_rate: ((this.ddh_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
        case "Dipole Drifts":
          this.occurrence_list.push({
            anomaly_type: anomaly_type.name,
            occurrence: this.dd_counter,
            occurrence_rate: ((this.dd_counter/filtered_anom.length) * 100).toFixed(2) + "%"
          });
          break;
      }
    }

    // Sort the table by occurrence
    this.occurrence_list.sort((anom1, anom2) => {
      if(anom1.occurrence < anom2.occurrence) {
        return 1;
      } else if(anom1.occurrence > anom2.occurrence) {
        return -1;
      } else {
        return 0;
      }
    })
  }
}