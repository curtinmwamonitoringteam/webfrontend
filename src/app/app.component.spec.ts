import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { APP_BASE_HREF } from '@angular/common';
import { NavigationModule } from './navigation/navigation.module';
import { AppRoutingModule } from './navigation/routing.module';
import { RoutingService } from './navigation/routing.service';

describe('App', () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppRoutingModule,
        NavigationModule,
      ],
      declarations: [
        AppComponent,
      ],
      providers: [
        RoutingService,
        {provide: APP_BASE_HREF, useValue: '/'},
      ],
    })
      .compileComponents();

    fixture = TestBed.createComponent(AppComponent);
  }));

  it('should work', () => {
    expect(fixture.componentInstance instanceof AppComponent).toBe(true, 'should create AppComponent');
  });

});
