import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './navigation/routing.module';
import { RoutingService } from './navigation/routing.service';
import { NavigationModule } from './navigation/navigation.module';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { httpFactory, HttpInterceptor } from './interceptor/http-interceptor';
import { RibbonModule } from './ribbon/ribbon.module';


@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    NavigationModule,
    RibbonModule,
  ],
  exports: [
    AppComponent,
  ],
  declarations: [
    AppComponent,
  ],
  bootstrap: [
    AppComponent,
  ],
  providers: [
    RoutingService,
    {provide: APP_BASE_HREF, useValue: '/'},
    {provide: HttpInterceptor, useFactory: httpFactory, deps: [XHRBackend, RequestOptions]},
  ]
})
export class AppModule {
}
