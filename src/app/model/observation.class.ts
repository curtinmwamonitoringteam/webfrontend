
export class Observation {

  public id: number;
  public name: string;
  public start_time: number;
  public dipole_id: number;
  public values: number[];

}
