
export class AnomalyType {

  public id: number;
  public name: string;
  public description: string;

}
