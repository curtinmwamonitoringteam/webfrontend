
import { Observation } from './observation.class';

export class Test {

  public id: number;
  public tile_id: number;
  public start_time: number;
  public observations: Observation[];

}