
export class Dipole {

  public id: number;
  public tile_id: number;
  public dipole_number: number;
  public polarity: number;

}
