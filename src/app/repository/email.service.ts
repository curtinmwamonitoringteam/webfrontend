import { Injectable } from '@angular/core';
import {Headers, Response} from '@angular/http';
import { HttpInterceptor } from '../interceptor/http-interceptor';
import { Observable } from 'rxjs/Observable';
import {Email} from "../model/email.class";


@Injectable()
export class EmailService {

  private url = '/email';

  constructor(private http: HttpInterceptor) {}

  getAllSubscribers() : Observable<Email[]> {
    return this.http.get(this.url)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  addSubscriber(email_address: string) : Observable<Response> {
    let body = "email_address=" + email_address;

    return this.http.post(this.url, body, {headers: new Headers({"Content-Type": "application/x-www-form-urlencoded"})})
  }

  deleteSubscriber(email_address: string) : Observable<Response> {
    let requestUrl = this.url + "/" + email_address;

    return this.http.delete(requestUrl)
  }
}
