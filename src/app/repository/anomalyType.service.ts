import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpInterceptor } from '../interceptor/http-interceptor';
import { AnomalyType } from "../model/anomaly-type.class";

@Injectable()
export class AnomalyTypeService {
  private url = '/anomaly-type';

  constructor(private http: HttpInterceptor) {}

  getAnomalyType(id: number): Observable<AnomalyType> {
    if(id > 0) {
      let requestUrl = this.url + "/" + id.toString();
      return this.http.get(requestUrl)
      // map response to json
        .map((resp: Response) => resp.json())
        // catch any errors
        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }
  }

  getAnomalyTypeByIdList(id_list: number[]): Observable<AnomalyType[]> {
    let params = new URLSearchParams();
    params.set('id_list', id_list.join(','));

    return this.http.get(this.url, {search: params})
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getAnomalyTypeAll(): Observable<AnomalyType[]> {
    return this.http.get(this.url)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error || "Server Error"));
  }
}