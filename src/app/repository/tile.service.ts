import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpInterceptor } from '../interceptor/http-interceptor';
import { Tile } from "../model/tile.class";

@Injectable()
export class TileService {

  private url = '/tile';

  constructor(private http: HttpInterceptor) {
  }

  getTileById(id: number) : Observable<Tile> {
    let requestUrl = this.url + '/' + id.toString();

    return this.http.get(requestUrl)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  getAllTiles() : Observable<Tile[]> {
    return this.http.get(this.url)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }
}