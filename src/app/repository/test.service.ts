import { Injectable } from '@angular/core';
import { Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Test } from '../model/test.class';
import { HttpInterceptor } from '../interceptor/http-interceptor';


@Injectable()
export class TestService {

  private url = '/test';

  constructor(private http: HttpInterceptor) {}

  getTestWithObservations(id: number): Observable<Test> {
    if (id > 0) {
      let requestUrl = this.url + '/' + id.toString();
      let params = new URLSearchParams();
      params.set('get_obs', 'true');

      return this.http.get(requestUrl, {search: params})
        .map((resp: Response) => resp.json())
        .map((test: Test) => {
          test.observations.map(obs => {
            obs.values = JSON.parse(obs.values as any);
            return obs;
          });
          return test;
        })
        .catch((error) => Observable.throw(error.json().error || 'Server Error'));
    }
  }

  getAllTest(): Observable<Test[]> {
    let params = new URLSearchParams();
    params.set('get_obs', 'true');

    return this.http.get(this.url, {search: params})
    // map the response to json
      .map((resp: Response) => resp.json() as Test[])
      // catch any errors
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  getTestsByDateWithObservations(gpsTime: number): Observable<Test[]> {
    if (gpsTime > 0) {
      let params = new URLSearchParams();
      params.set('start_time', gpsTime.toString());
      params.set('get_obs', 'true');

      return this.http.get(this.url, {search: params})
        .map((resp: Response) => resp.json())
        .map((tests: Test[]) => {
          for (let test of tests) {
            test.observations.map(obs => {
              obs.values = JSON.parse(obs.values as any);
              return obs;
            });
          }
          return tests;
        })
        .catch((error) => Observable.throw(error.json().error || 'Server Error'));
    }
  }

  getTestsByDateRangeWithObservations(startTime: number, endTime: number): Observable<Test[]> {
    if (startTime > 0 && endTime > 0 && endTime >= startTime) {
      let params = new URLSearchParams();
      params.set('start_time', startTime.toString());
      params.set('end_time', endTime.toString());
      params.set('get_obs', 'true');

      return this.http.get(this.url, {search: params})
        .map((resp: Response) => resp.json())
        .map((tests: Test[]) => {
          for (let test of tests) {
            test.observations.map(obs => {
              obs.values = JSON.parse(obs.values as any);
              return obs;
            });
          }
          return tests;
        })
        .catch((error) => Observable.throw(error.json().error || 'Server Error'));
    }
  }

  getTestsByIdRangeWithObservations(start_id: number, end_id: number) {
    if(start_id > 0 && end_id > 0 && end_id >= start_id) {
      let params = new URLSearchParams();
      params.set('start_id', start_id.toString());
      params.set('end_id', end_id.toString());
      params.set('get_obs', 'true');

      return this.http.get(this.url, {search: params})
        .map((resp: Response) => resp.json())
        .map((tests: Test[]) => {
          for (let test of tests) {
            test.observations.map(obs => {
              obs.values = JSON.parse(obs.values as any);
              return obs;
            });
          }
          return tests;
        })
        .catch((error) => Observable.throw(error.json().error || 'Server Error'));
    }
  }
}
