import { Injectable } from '@angular/core';
import { Response, URLSearchParams } from '@angular/http';
import { HttpInterceptor } from '../interceptor/http-interceptor';
import { Dipole } from '../model/dipole.class';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class DipoleService {

  private url = '/dipole';

  constructor(private http: HttpInterceptor) {}

  getDipoles(tile_id: number): Observable<Dipole[]> {
    let requestUrl = this.url;
    let params = new URLSearchParams();

    params.set('tile_id', tile_id.toString());

    return this.http.get(requestUrl, {search: params})
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  getDipoleById(id: number): Observable<Dipole> {
    if(id > 0) {
      let requestUrl = this.url + '/' +  id.toString();
      return this.http.get(requestUrl)
      // map response to json
        .map((resp: Response) => resp.json())
        // catch any errors
        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }
  }

  getAllDipoles(): Observable<Dipole[]> {
    return this.http.get(this.url)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }
}
