import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpInterceptor } from '../interceptor/http-interceptor';
import {Observation} from "../model/observation.class";


@Injectable()
export class ObservationService {

  private url = '/observation';

  constructor(private http: HttpInterceptor) {
  }

  getObservationById(id: number): Observable<Observation> {
    let requestURL = this.url + '/' + id.toString();

    return this.http.get(requestURL)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  getAllObservations(): Observable<Observation[]> {
    return this.http.get(this.url)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
}
}