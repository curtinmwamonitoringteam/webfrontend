import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpInterceptor } from '../interceptor/http-interceptor';
import { Anomaly } from "../model/anomaly.class";

@Injectable()
export class AnomalyService {
  private url = '/anomaly';

  constructor(private http: HttpInterceptor) {
  }

  getAnomaly(): Observable<Anomaly[]> {
    return this.http.get(this.url)
    // map response to json
      .map((resp: Response) => resp.json())
      // catch any errors
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  getAnomaliesFromObsIDs(obs_id_list: number[]): Observable<Anomaly[]> {
    let params = new URLSearchParams();
    params.set('obs_id_list', obs_id_list.join(','));

    return this.http.get(this.url, {search: params})
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }
}